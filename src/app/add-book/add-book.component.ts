import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Book, Category } from '../model/book.model';
import { BookService } from '../service/book.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  public book: Book;
  private categories: Array<string>;

  @ViewChild('addBookForm') public addBookForm: NgForm;

  constructor(private bookService: BookService) {
    const keys = Object.keys(Category);
    this.categories = keys.slice(keys.length / 2);
  }

  public ngOnInit() {
    this.initializeModel();
  }

  private initializeModel() {
    this.book = <Book>{};
  }

  public onSubmit(bookForm: NgForm) {
    this.bookService.createBook(this.book);
    this.initializeModel();
    bookForm.resetForm();
  }

  get Categories() {
    return this.categories;
  }
}
