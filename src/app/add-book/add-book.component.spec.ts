import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AddBookComponent } from './add-book.component';
import { CustomMaterialModule } from '../material.module';

describe('AddBookComponent', () => {
  let component: AddBookComponent;
  let fixture: ComponentFixture<AddBookComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddBookComponent],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        CustomMaterialModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a title of Add Book in h3 tag', () => {
    expect(compiled.querySelector('h3').textContent).toContain('Add Book');
  });
  it('should have book title, category and description user input controls', () => {
    expect(compiled.querySelector('input[name="title"]')).toBeTruthy();
    expect(compiled.querySelector('mat-select[name="category"]')).toBeTruthy();
    expect(compiled.querySelector('textarea[name="description"]')).toBeTruthy();
  });
  it('should ensure title is mandatory', () => {
    fixture.whenStable().then(() => {
      expect(component.addBookForm.form.controls['title'].valid).toBeFalsy();
      expect(component.addBookForm.form.controls['category'].valid).toBeFalsy();
      expect(component.addBookForm.form.controls['description'].valid).toBeFalsy();

      const titleEle = compiled.querySelector('input[name="title"]');
      titleEle.value = 'War and Peace';
      titleEle.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(component.addBookForm.form.controls['title'].valid).toBeTruthy();
    });
  });
});
