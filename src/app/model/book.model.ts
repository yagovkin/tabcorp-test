export enum Category {
    Drama,
    Comedy,
    Sport
}

export interface Book {
    title: string;
    category: Category;
    description: string;
}
