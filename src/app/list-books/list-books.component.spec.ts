import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { ListBooksComponent } from './list-books.component';
import { BookService } from '../service/book.service';
import { CustomMaterialModule } from '../material.module';

describe('ListBooksComponent', () => {
  let component: ListBooksComponent;
  let fixture: ComponentFixture<ListBooksComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListBooksComponent],
      imports: [
        CustomMaterialModule
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render a title of Book List in h3 tag', () => {
    expect(compiled.querySelector('h3').textContent).toContain('Book List');
  });
  it('should render a title of Book List (1) in h3 tag', async(async () => {
    const bookService = getTestBed().get(BookService);
    bookService.createBook({
      title: 'War and Peace',
      category: 'Drama',
      description: 'By Leo Tolstoy'
    });
    await fixture.whenStable();
    fixture.detectChanges();
    expect(compiled.querySelector('h3').textContent).toContain('Book List (1)');
  }));
});
