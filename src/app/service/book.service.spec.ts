import { TestBed, inject } from '@angular/core/testing';

import { BookService } from './book.service';
import { Book, Category } from '../model/book.model';

describe('BookService', () => {
  let book1: Book;
  let book2: Book;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookService]
    });

    book1 = {
      title: 'War and Peace',
      category: Category.Drama,
      description: 'By Leo Tolstoy'
    };
    book2 = {
      title: 'Moby-Dick',
      category: Category.Sport,
      description: 'The Whale is an 1851 novel'
    };
  });

  it('should be created', inject([BookService], (service: BookService) => {
    expect(service).toBeTruthy();
  }));
  it('should initialized with an empty bookStore', inject([BookService], (service: BookService) => {
    expect(service.count).toEqual(0);
  }));

  describe('createBook(), getBooks()', () => {
    it('should add books to the array', inject([BookService], (service: BookService) => {
      service.createBook(book1);
      expect(service.count).toEqual(1);
    }));
    it('should return all books', inject([BookService], (service: BookService) => {
      service.createBook(book1);
      service.createBook(book2);
      expect(service.count).toEqual(2);
      expect(service.getBooks[0].title).toEqual('War and Peace');
      expect(service.getBooks[1].title).toEqual('Moby-Dick');
    }));
  });
});
