import { Injectable } from '@angular/core';
import { Book } from '../model/book.model';

@Injectable({
  providedIn: 'root'
})
// There is no integration with API, all the books are stored in and retrieved from an array.
export class BookService {
  private books: Array<Book>;

  constructor() {
    this.books = [];
  }

  public createBook(book: Book) {
    this.books.push(book);
  }

  get getBooks() {
    return this.books;
  }

  get count() {
    return this.books.length;
  }
}
